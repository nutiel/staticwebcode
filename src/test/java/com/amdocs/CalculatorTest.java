package com.amdocs;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest {
    
    @Test
    public void runTestAdd() throws Exception {

        int k = new Calculator().add();

        assertEquals("Add", 9, k);
    }

    @Test
    public void runTestSub() throws Exception {

        int k = new Calculator().sub();

        assertEquals("Add", 3, k);
    }
}
