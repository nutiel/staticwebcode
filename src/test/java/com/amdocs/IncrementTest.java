package com.amdocs;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class IncrementTest {
    
    @Test
    public void runTestDecreaseCounter() throws Exception {

        Increment incr = new Increment();

        int counter = incr.getCounter();
        int k = incr.decreasecounter(0);

        assertEquals("counter0", counter+1, k);

        counter = incr.getCounter();
        k = incr.decreasecounter(1);

        assertEquals("counter1", counter+1, k);

        counter = incr.getCounter();
        k = incr.decreasecounter(2);

        assertEquals("counter2", counter+1, k);
    }
}
